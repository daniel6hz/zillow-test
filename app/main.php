<?php

/**
 * Zillow Coding Test
 * reference: http://www.zillow.com/howto/api/GetSearchResults.htm
 *
 * PHP version 5
 *
 * @author     Daniel Liu <daniel.lhz@gmail.com>
 * @version    SVN: $Id$
 */

?>

<br>
<br>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<form method="post" style="text-align: center;">
<input type="text" name="address" id="address" placeholder="address"  required="true"/>
<input type="text" name="city" id="city" placeholder="city"  required="true"/>
<input type="text" name="state" id="state" placeholder="state"  required="true"/>
<input type="submit" value="OK" />  
</form>

<?php

// handle curl with url and params
function httpPost($url,$params)
{
  $postData = '';
   //create name value pairs seperated by &
   foreach($params as $k => $v) 
   { 
      $postData .= $k . '='.$v.'&'; 
   }
   $postData = rtrim($postData, '&');
 
    $ch = curl_init();  
 
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
    $output=curl_exec($ch);
 
    curl_close($ch);

    return $output;
 
}

// process array to echo as unordered list 
function processArray($in) {
 echo "<ul>";
 foreach($in AS $key => $value) {
  if(is_array($value)) {
   echo "<li>$key:</li>";
   processArray($value);
  } else {
   echo "<li>$key:$value</li>";
  }
 }
 echo "</ul>";
}

// double check to make sure all 3 fields are set
if( !empty($_POST) && $_POST["address"] && $_POST["city"] &&  $_POST["state"]) {
    $params=[
      'zws-id' => 'X1-ZWz1dyb53fdhjf_6jziz',
      'address' => $_POST['address'],
      'citystatezip'   => $_POST['city'] . ' ' . $_POST['state']
    ];

    $obj = httpPost("http://www.zillow.com/webservice/GetSearchResults.htm",$params);
    $oXML = new SimpleXMLElement($obj);

    // According to api manul "Messages and Codes", in case any errors, should show message->text
    if (empty($oXML->response)) {
      echo "<div style='text-align: center;'>" . $oXML->message->text . "</div>";
      exit();
    }
    
    $json = json_encode($oXML->response->results->result);
    $arr = json_decode($json,TRUE);
 
    processArray($arr);   
}

?>