# README #

This is the coding test for zillow interview

# Comments #
* Deployed on AWS EC2 instance.
* Use "whoops" to help me debug.
* Use "Bootstrap"
* No Unit Test
* Only basic exception handling

I tried to set everything as basic as possible.
Bootstrap is used to perform the form validation and better layout.

Please checkout app/main.php

# Test #
Please use [52.35.209.63](http://52.35.209.63) to test.

# Contact #
Please contact me by daniel.lhz@gmail.com